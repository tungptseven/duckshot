// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class DuckControl extends cc.Component {
    speed: number
    gameLayout: GameLayout
    gameControl: GameScene = null

    private _isShot: boolean = false

    public get isShot(): boolean {
        return this._isShot
    }

    public set isShot(value: boolean) {
        if (!this._isShot) {
            this._isShot = value
        }
        this.onDeath()
        switch (this.node.name) {
            case 'Duck1':
                this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
                this.gameLayout.gameScore -= 1
                break
            case 'Duck2':
                this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Shot)
                this.gameLayout.gameScore += 1
                break
            case 'Duck3':
                this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Shot)
                this.gameLayout.gameScore += 2
                break
            case 'Duck4':
                this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Shot)
                this.gameLayout.gameScore += 1
                break
        }
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        let maxSpeed = 4
        let minSpeed = 2
        this.speed = Math.floor(minSpeed + Math.random() * (maxSpeed + 1 - minSpeed))
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
    }

    onDeath() {
        this.node.getChildByName('PointLbl').active = true
        this.node.getComponent(cc.PolygonCollider).enabled = false
        setTimeout(() => {
            if (this.node) { this.node.destroy() }
        }, 300)
    }

    start() {
        this.node.runAction(cc.moveTo(this.speed, cc.v2(800, this.node.y)))
    }

    update(dt) {
        if (this.node.x >= 800) {
            this.speed = 0
            this.node.destroy()
        }
    }
}
