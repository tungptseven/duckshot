// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

import DuckControl from "./DuckControl"

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameOverLayout = null
    time: number = 300
    timeProgress = null
    progress
    timeCheck = [0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165, 180, 195, 210, 235, 240, 255, 270, 285]
    duckCd: number = 1
    gameScore: number = 0

    duckList = []
    _arrDuck: cc.PolygonCollider[] = []

    @property(cc.Prefab)
    duck1: cc.Prefab = null
    @property(cc.Prefab)
    duck2: cc.Prefab = null
    @property(cc.Prefab)
    duck3: cc.Prefab = null
    @property(cc.Prefab)
    duck4: cc.Prefab = null

    @property(cc.Label)
    scoreLbl: cc.Label = null

    wave1: cc.Node
    wave2: cc.Node
    // LIFE-CYCLE CALLBACKS:

    _registerEvent() {
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_START, this._onDragBegan, this)
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_MOVE, this._onDragMoved, this)
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_END, this._onDragEnded, this)
        cc.Canvas.instance.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onDragCancelled, this)
    }

    _unregisterEvent() {
        cc.Canvas.instance.node.off(cc.Node.EventType.TOUCH_START, this._onDragBegan, this)
        cc.Canvas.instance.node.off(cc.Node.EventType.TOUCH_MOVE, this._onDragMoved, this)
        cc.Canvas.instance.node.off(cc.Node.EventType.TOUCH_END, this._onDragEnded, this)
        cc.Canvas.instance.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onDragCancelled, this)
    }

    _onDragBegan(event: cc.Event.EventTouch) {
        // if (GameManager.instance.bullet) {
        let touchLocation = event.getLocation()
        // this.nTarget.active = true
        // this.nTarget.setPosition(this.nTarget.parent.convertToNodeSpaceAR(touchLocation))
        // }
    }

    _onDragMoved(event: cc.Event.EventTouch) {

        let touchLocation = event.getLocation()
        // this.nTarget.setPosition(this.nTarget.parent.convertToNodeSpaceAR(touchLocation))
    }

    _onDragEnded(event: cc.Event.EventTouch) {
        // if (this.nTarget.active) {
        let touchLocation = event.getLocation()
        // this.nTarget.active = false
        this.onFire(touchLocation)
        // }
    }

    _onDragCancelled(event: cc.Event.EventTouch) {

        // this.nTarget.active = false
    }

    onLoad() {
        cc.director.getCollisionManager().enabled = true

        this.timeProgress = this.node.getChildByName('Time').getChildByName('timeProgress').getComponent(cc.Sprite)
        this.progress = 1
        this.timeProgress.fillRange = this.progress

        this.duckList = [this.duck1, this.duck2, this.duck3, this.duck4]

        this.wave1 = this.node.getChildByName('wave1')
        this.wave2 = this.node.getChildByName('wave2')
        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')

    }

    onSpawn() {
        let duck = cc.instantiate(this.duckList[Math.floor(0 + Math.random() * (3 + 1 - 0))])
        let maxY = 0
        let minY = -150
        duck.y = Math.floor(minY + Math.random() * (maxY + 1 - minY))
        duck.x = -800
        this.node.getChildByName('Ducks').addChild(duck)
        this._arrDuck.push(duck.getComponent(cc.PolygonCollider))
    }

    start() {

    }

    onFire(pos: cc.Vec2) {
        for (let duck of this._arrDuck) {
            if (duck && duck.node) {
                let localPos = duck.node.convertToNodeSpaceAR(pos)
                if (cc.Intersection.pointInPolygon(localPos, duck.points)) {
                    let com = duck.node.getComponent('DuckControl') as DuckControl
                    com.isShot = true
                }
            }
        }
    }

    runWave() {
        this.wave1.position = cc.v2(123, -211)
        this.wave2.position = cc.v2(-14, -248)
        this.wave1.runAction(cc.repeatForever(
            cc.sequence(
                cc.moveTo(4, 0, this.wave1.y),
                cc.moveTo(4, 123, this.wave1.y),
            )))
        this.wave2.runAction(cc.repeatForever(
            cc.sequence(
                cc.moveTo(2, -137, this.wave2.y),
                cc.moveTo(2, -14, this.wave2.y),
            )))
    }


    onPlay(isReplay: boolean = true) {
        this.time = 300
        this.progress = 1
        this.timeProgress.fillRange = this.progress
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = 0
        this.scoreLbl.string = this.gameScore.toString()
        this.onSpawn()
        this.countdown(this.time)
        this._registerEvent()
        this.runWave()
    }


    gameOver() {
        this.gameStatus = GameStatus.Game_Over
        cc.Canvas.instance.node.getChildByName('GameLayout').active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        this.node.getChildByName('Ducks').destroyAllChildren()
        this.wave1.stopAllActions()
        this.wave2.stopAllActions()
    }

    countdown(time: number) {
        --time
        if (time > 0) {
            this.scheduleOnce(() => this.countdown(time), 1)
            if (this.timeCheck.indexOf(time) !== -1) {
                this.progress -= 0.05
                this.timeProgress.fillRange = this.progress

            }
        } else {
            this.gameOver()
        }
    }

    update(dt) {
        if (this.gameStatus === GameStatus.Game_Playing) {
            this.duckCd -= dt
            if (this.duckCd <= 0) {
                this.onSpawn()
                this.duckCd = 1
            }
            if (this.gameScore <= 0) {
                this.gameScore = 0
            }
            this.scoreLbl.string = this.gameScore.toString()

        }
    }
}
